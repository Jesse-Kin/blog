---
title: 使用 Docker 安装 MySQL
date: 2024-03-04 15:51:46
updated: 2024-03-05 11:02:56
tags:
---

1.拉取最新的镜像

```sh
docker pull mysql:latest
```

2.查看镜像

```sh
docker images
```

3.运行容器

```sh
docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql
```

4.查看运行状态

```sh
docker ps
```

5.登录mysql

```sh
mysql -h localhost -u root -p
```

启动已存在的容器

```sh
docker start 容器名
```
