---
title: Windows 电脑安装 Oh My Posh
date: 2024-03-04 15:14:01
updated: 2024-03-05 11:02:56
tags:
---
## 安装 Oh My Posh

官网地址：<https://ohmyposh.dev/>

使用winget安装

```ps
winget install JanDeDobbeleer.OhMyPosh -s winget
```
