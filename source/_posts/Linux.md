---
title: Linux 防火墙开启某个端口
date: 2024-03-15 15:03:47
updated: 2024-03-15 15:03:47
tags:
---

永久开放例如 3306/tcp 端口

```sh
firewall-cmd --permanent --add-port=3306/tcp
```

重载防火墙策略

```sh
firewall-cmd --reload 
```

查看防火墙策略

```sh
firewall-cmd --list-all
```
