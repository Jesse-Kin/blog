---
title: GitLab Page + Hexo
date: 2024-03-03 16:44:12
updated: 2024-03-05 11:02:56
tags:
---
## 环境搭建

- 注册 GitLab 账号(需要使用 GitLab Page 部署静态网页); <https://gitlab.com/>
- 安装 Node (软件包管理工具); <https://nodejs.org/en>
- 安装 Git (版本控制工具); <https://git-scm.com/>

 全局安装 hexo-cli

```powershell
npm install hexo-cli -g
```

检查是否安装成功

```powershell
hexo -v
```

初始化 hexo

```powershell
hexo init <blog>
```

进入目录

```powershell
cd blog
```

安装依赖模块

```powershell
npm install
```

新建关于页

```sh
hexo new page --path about/me "About me"
```

安装 hexo-server

```sh
npm install hexo-server --save
```

修改 _config.yml 中,用于生成同名文件夹

```yml
post_asset_folder: true
```

博客标题

```yml
blog_title: "Jesse的博客"
```

首页 Slogan

```yml
index:
  slogan:
    enable: true

    text: "Please type your slogan here."
    api:
      enable: true
      url: "https://v1.hitokoto.cn/"
      method: "GET"
      headers: {}
      keys: ["hitokoto"]
```

永久链接配置 _config.yml
permalink_defaults

```yml
permalink_defaults:
  lang: en
```

创建菜单

打开主题配置文件 _config.next.yml

取消需要打开的菜单前的注释

```yml
menu:
  #home: / || fa fa-home
  #about: /about/ || fa fa-user
  #tags: /tags/ || fa fa-tags
  #updated: 2024-03-15 15:45:07
categories: /categories/ || fa fa-th
  #archives: /archives/ || fa fa-archive
  #schedule: /schedule/ || fa fa-calendar
  #sitemap: /sitemap.xml || fa fa-sitemap
  #commonweal: /404/ || fa fa-heartbeat
```

修改为：

```yml
menu:
  home: / || fa fa-home
  about: /about/ || fa fa-user
  #tags: /tags/ || fa fa-tags
  #updated: 2024-03-15 15:45:07
categories: /categories/ || fa fa-th
  #archives: /archives/ || fa fa-archive
  #schedule: /schedule/ || fa fa-calendar
  #sitemap: /sitemap.xml || fa fa-sitemap
  #commonweal: /404/ || fa fa-heartbeat
```

添加搜索功能

```sh
npm install hexo-generator-searchdb
```

打开本地搜索

```yml
local_search:
  enable: false
```

```yml
local_search:
  enable: true
```

隐藏强力驱动
node_modules\hexo-theme-next\layout\_partials\footer.njk
